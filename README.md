## developer.gitlab.com

![Pipeline Status](https://gitlab.com/gitlab-org/developer.gitlab.com/badges/main/pipeline.svg)

### Overview

This is the project for the developer portal located at https://developer.gitlab.com. The goal of this project is to create a centralized resource for developers looking for information about contributing to, or integrating with, the GitLab Project.

### Development

To get started:

1. Run `yarn && yarn start`
1. ???
1. Profit

### Contributing

Contributions are welcome!

### License

The developer.gitlab.com is published under the Creative Commons BY-SA 4.0 license; see https://creativecommons.org/licenses/by-sa/4.0/ for more information.